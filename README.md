事成项目管理10大模板Excel版(可直接套用_非常实用)<br>
事成项目管理<br>
753487618<br>
1<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/100636_a3fef831_132236.png "屏幕截图.png")<br>
2<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/100646_9abb50ff_132236.png "屏幕截图.png")<br>
3<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/100653_86cad591_132236.png "屏幕截图.png")<br>
4<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/100702_404d0ee7_132236.png "屏幕截图.png")<br>
5<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/100710_46287a84_132236.png "屏幕截图.png")<br>
6<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/100719_09b4e4c0_132236.png "屏幕截图.png")<br>
7<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/100728_4a401c11_132236.png "屏幕截图.png")<br>
8<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/100737_59d03500_132236.png "屏幕截图.png")<br>
9<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/100744_7dad8729_132236.png "屏幕截图.png")<br>
10<br>
![输入图片说明](https://images.gitee.com/uploads/images/2021/0805/100754_29d481fa_132236.png "屏幕截图.png")<br>

项目管理是管理学的一个分支学科 ，对项目管理的定义是：指在项目活动中运用专门的知识、技能、工具和方法，使项目能够在有限资源限定条件下，实现或超过设定的需求和期望的过程。项目管理是对一些与成功地达成一系列目标相关的活动（譬如任务）的整体监测和管控。这包括策划、进度计划和维护组成项目的活动的进展。
“项目是在限定的资源及限定的时间内需完成的一次性任务。具体可以是一项工程、服务、研究课题及活动等。”
“项目管理是运用管理的知识、工具和技术于项目活动上，来达成解决项目的问题或达成项目的需求。所谓管理包含领导（leading）、组织（organizing）、用人（staffing）、计划（planning）、控制（controlling）等五项主要工作。”
项目管理（Project Management）：运用各种相关技能、方法与工具，为满足或超越项目有关各方对项目的要求与期望，所开展的各种计划、组织、领导、控制等方面的活动。


https://gitee.com/JeeHuangBingGui/opensoftproject/wikis/%E7%94%98%E7%89%B9%E5%9B%BE?sort_id=4316876
甘特图
燃尽图（burn down chart）
WBS（工作结构）分解图
HOQ用于定义顾客预期和公司能力之间的关系。
RACI图：这个图表最大的作用就是在项目进行的过程中，用于记录不同角色之间的责任，其中R代表负责执行的人，A代表批准的人、C代表可以完成项目的人员、I代表应该被通知的人
矩阵组织图把按职能划分的部门和按产品（或项目、服务等）划分的部门结合起来组成一个矩阵，是同一名员工既同原职能部门保持组织与业务上的联系，又参加产品或项目小组的工作的一种结构
PERT（计划评审技术）图利用网络分析制定计划以及对计划予以评价的技术。它能协调整个计划的各道工序，合理安排人力、物力、时间、资金，加速计划的完成。
在现代计划的编制和分析手段上，PERT被广泛地使用，是现代项目管理的重要手段和方法。
思维导图
在项目初期什么都没有定下来的时候，用思维导图将项目全状进行一个展示，不但方便修改，也能在后期迅速的生成对应的图表，这个应该在互联网等敏捷性开发项目中应用的较多。